import React from 'react';
import Layout from './layout/Layout';
import NewsList from './NewsList';

const Feed = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1 style={{ fontSize: 60, height: 10, justifyContent: 'end' }}>
          HN Feed
        </h1>
        <p
          style={{ fontSize: 20, paddingBottom: 10, justifyContent: 'start' }}
        >{`We <3 hacker news!`}</p>
      </header>
      <Layout>
        <NewsList></NewsList>
      </Layout>
    </div>
  );
};

export default Feed;
