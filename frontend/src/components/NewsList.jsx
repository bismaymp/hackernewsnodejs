import React, { useState, useEffect } from 'react';
import Card from './Card';
import axios from 'axios';
import { ARTICLES_ENDPOINT, POPULATE_ENDPOINT } from '../constants';
import { RiRefreshFill } from 'react-icons/ri/index';

const ListStyle = {
  height: '500px',
  overflow: 'auto',
  display: 'block',
};

const NewsList = ({ news, ...rest }) => {
  const [state, setState] = useState([]);

  useEffect(() => {
    getArticles();
  }, []);

  const handleDelete = async (id) => {
    axios
      .put(`${ARTICLES_ENDPOINT}/${id}`)
      .then(async (res) => await getArticles())
      .catch((error) => console.log(error));
  };

  const getArticles = async () => {
    axios
      .get(ARTICLES_ENDPOINT)
      .then((res) => {
        const articles = res.data;
        setState(articles);
      })
      .catch((error) => console.log(error));
  };

  const handleRefresh = () => {
    axios
      .get(POPULATE_ENDPOINT)
      .then(async (res) => {
        await getArticles();
      })
      .catch((error) => console.log(error));
  };

  return (
    <>
      <button onClick={handleRefresh}>
        <RiRefreshFill /> Populate Db and refresh list
      </button>
      <ul style={ListStyle}>
        {state
          .sort(
            (item1, item2) =>
              new Date(item2.created_at) - new Date(item1.created_at)
          )
          .map((article) => (
            <Card
              key={article.id}
              article={article}
              handleDelete={handleDelete}
            />
          ))}
      </ul>
    </>
  );
};

export default NewsList;
