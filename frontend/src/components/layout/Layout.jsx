import React from 'react';

const LayoutStyle = {
  paddingTop: 40,
  paddingRight: 50,
  paddingLeft: 50,
  paddingBottom: 40,
};

const Layout = ({ children }) => {
  return <div style={LayoutStyle}>{children}</div>;
};

export default Layout;
