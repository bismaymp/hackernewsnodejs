import React, { useState } from 'react';
import { isSameDay, subDays } from 'date-fns';
import { RiDeleteBin6Line } from 'react-icons/ri/index';

const TitleStyle = {
  fontSize: '13pt',
  color: `#333`,
};

const CreatedAtStyle = {
  fontSize: '13pt',
  color: `#333`,
  paddingRight: 60,
};

const AuthorStyle = {
  color: `#999`,
};

const LeftSectionsStyle = {
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  paddingLeft: 20,
  flex: 1,
  with: '',
};

const RightSectionsStyle = {
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  paddingRight: 30,
  with: '100%',
};

const Card = ({ article, handleDelete, ...rest }) => {
  const [showDelete, setShowDelete] = useState(false);
  const formatDate = (dateStr) => {
    const date = new Date(dateStr);
    const today = new Date();
    const yesterday = subDays(today, 1);

    if (isSameDay(date, today)) {
      return Intl.DateTimeFormat('en', {
        hour: 'numeric',
        minute: 'numeric',
        hour12: true,
      }).format(date); // 2:00 PM
    }
    if (isSameDay(date, yesterday)) {
      return 'Yesterday';
    } else {
      return date.toLocaleDateString('en-EN', {
        month: 'short',
        day: '2-digit',
      });
    }
  };

  const handleDeleteClick = async () => {
    await handleDelete(article.id);
  };

  const handleOnClick = () => {
    window.open(
      article.story_url || article.url,
      '_blank',
      'noopener,noreferrer'
    );
  };

  return (
    <li
      className={'Card'}
      onMouseEnter={() => setShowDelete(true)}
      onMouseLeave={() => setShowDelete(false)}
    >
      <div style={LeftSectionsStyle} onClick={handleOnClick}>
        <p style={TitleStyle}>{article.story_title || article.title}</p>
        <p style={AuthorStyle}> - {article.author} - </p>
      </div>
      <div style={RightSectionsStyle}>
        <p style={CreatedAtStyle} onClick={handleOnClick}>
          {formatDate(article.created_at)}
        </p>
        {showDelete && (
          <label onClick={handleDeleteClick}>
            <RiDeleteBin6Line style={{ cursor: 'pointer' }} />
          </label>
        )}
      </div>
    </li>
  );
};
export default Card;
