import * as dotenv from 'dotenv'
dotenv.config()

const API = process.env.API || 'http://127.0.0.1:8080'

export const POPULATE_ENDPOINT = `${API}/api/articles/populate`
export const ARTICLES_ENDPOINT = `${API}/api/articles`