import { HttpService, Injectable } from '@nestjs/common';
import { Article } from './articles.model';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Cron } from '@nestjs/schedule';
import { FormattedArticle } from './articles.model';

@Injectable()
export class ArticlesService {
  private articles: Article[] = [];

  constructor(
    @InjectModel('Article') private readonly articleModel: Model<Article>,
    private readonly httpService: HttpService,
  ) {}

  async find(): Promise<Article[]> {
    return await this.articleModel.find().exec();
  }

  async insert(article: Article): Promise<string> {
    const newArticle = new this.articleModel(article);
    const response = await newArticle.save();
    return response._id;
  }

  async updateById(id: string, body: object): Promise<Article> {
    return await this.articleModel.findByIdAndUpdate(id, body).exec();
  }

  async fetchArticlesFromHN(): Promise<any> {
    return await (
      await this.httpService
        .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .toPromise()
    ).data;
  }

  async insertArticle(article: Article): Promise<string> {
    return await this.insert(article);
  }

  async getFormattedArticles(): Promise<FormattedArticle[]> {
    const articles = await this.find();
    return articles
      .filter((item) => !item.deleted)
      .map((art) => ({
        id: art.id,
        created_at: art.created_at,
        title: art.title,
        url: art.url,
        author: art.author,
        story_title: art.story_title,
        story_url: art.story_url,
      }));
    
  }

  async deleteArticle(id: string): Promise<string> {
    const res = await this.updateById(id, { deleted: true });
    if (res._id) {
      return 'Article deleted successfully';
    }
    return 'Error deleting the article';
  }

  @Cron('* 30 * * * *')
  async populateDB(): Promise<number> {
    let cont = 0;
    const currentArticles = await this.find();
    const fetchedArticles = await this.fetchArticlesFromHN();
    if (fetchedArticles?.hits.length > 0) {
      fetchedArticles.hits.map(async (item: any) => {
        const res = currentArticles.find(
          (article) => article.title === item.title &&
            article.story_title === item.story_title &&
            article.author === item.author &&
            article.url === item.url &&
            article.story_url === item.story_url
        );
        if (!res) {
          cont++;
          await this.insertArticle(item);
          
        }
      });
    }
    return cont;
  }
}
