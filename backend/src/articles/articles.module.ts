import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleSchema } from './articles.model';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
    HttpModule.register({
        timeout: 5000,
        maxRedirects: 5
    })
  ],
  controllers: [ArticlesController],
  providers: [ArticlesService],
})
export class ArticlesModule {}
