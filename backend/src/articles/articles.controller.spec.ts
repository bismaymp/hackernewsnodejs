import { Test } from '@nestjs/testing';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/common';
import { ArticleSchema } from './articles.model';
import { MongoDBAtlas, MongoDBConnectionPath } from '../app.module';
import * as dotenv from 'dotenv';
dotenv.config();

describe('ArticleController', () => {
  let articlesController: ArticlesController;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(
          process.env.MONGO_HOST === 'atlas'
            ? MongoDBAtlas
            : MongoDBConnectionPath,
        ),
        MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
        HttpModule.register({
          timeout: 5000,
          maxRedirects: 5,
        }),
      ],
      controllers: [ArticlesController],
      providers: [ArticlesService],
    }).compile();

    articlesController = moduleRef.get<ArticlesController>(ArticlesController);
  });

  describe('Defined modules', () => {
    it('articlesController should be defined', () => {
      expect(articlesController).toBeDefined();
    });
  });
});
