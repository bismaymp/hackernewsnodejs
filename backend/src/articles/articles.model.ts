import * as mongoose from 'mongoose';

export const ArticleSchema = new mongoose.Schema({
  created_at: { type: Date, require: true },
  title: { type: String, require: false },
  url: { type: String, require: false },
  author: { type: String, require: false },
  story_text: { type: String, require: false },
  comment_text: { type: String, require: false },
  num_comments: { type: Number, require: false },
  story_id: { type: Number, require: false },
  story_title: { type: String, require: false },
  story_url: { type: String, require: false },
  parent_id: { type: Number, require: false },
  created_at_i: { type: Number, require: false },
  _tags: { require: false, type: [String] },
  objectID: { type: Number, require: false },
  _highlightResult: {
    require: false,
    type: {
      author: {
        require: false,
        type: {
          value: { type: String, require: false },
          matchLevel: { type: String, require: false },
          matchedWords: { require: false, type: [String] },
        },
      },
      comment_text: {
        require: false,
        type: {
          value: { type: String, require: false },
          matchLevel: { type: String, require: false },
          fullyHighlighted: { require: false, type: Boolean },
          matchedWords: { require: false, type: [String] },
        },
      },
      story_title: {
        require: false,
        type: {
          value: { type: String, require: false },
          matchLevel: { type: String, require: false },
          matchedWords: { require: false, type: [String] },
        },
      },
      story_url: {
        require: false,
        type: {
          value: { type: String, require: false },
          matchLevel: { type: String, require: false },
          matchedWords: { require: false, type: [String] },
        },
      },
    },
  },
  deleted: { type: Boolean, defaultStatus: false },
});

export interface Article extends mongoose.Document {
  id: string;
  created_at: Date;
  title: string;
  url: string;
  author: string;
  story_text: string;
  comment_text: string;
  num_comments: number;
  story_id: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: number;
  _tags: Array<string>;
  objectID: number;
  _highlightResult: {
    author: {
      value: string;
      matchLevel: string;
      matchedWords: Array<string>;
    };
    comment_text: {
      value: string;
      matchLevel: string;
      fullyHighlighted: boolean;
      matchedWords: Array<string>;
    };
    story_title: {
      value: string;
      matchLevel: string;
      matchedWords: Array<string>;
    };
    story_url: {
      value: string;
      matchLevel: string;
      matchedWords: Array<string>;
    };
  };
  deleted: boolean;
}

export interface FormattedArticle {
  id: string;
  created_at: Date;
  title: string;
  url: string;
  author: string;
  story_title: string;
  story_url: string;
}
