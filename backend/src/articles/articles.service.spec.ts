import { Article, ArticleSchema, FormattedArticle } from './articles.model';
import { HttpService, HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { Model } from 'mongoose';
import { MongooseModule } from '@nestjs/mongoose';
import { MongoDBAtlas, MongoDBConnectionPath } from '../app.module';

describe('ArticleController', () => {
  let articleService: ArticlesService;
  const articlesArray = [
    {
      id: '1',
      created_at: new Date('2021-02-13T22:50:52.074Z'),
      title: '1',
      url: '1',
      author: '1',
      story_title: '1',
      story_url: '1',
      deleted: true,
    },
    {
      id: '2',
      created_at: new Date('2021-02-13T22:50:52.074Z'),
      title: '1',
      url: '1',
      author: '1',
      story_title: '1',
      story_url: '1',
      deleted: false,
    },
  ];

  const HNArticles = {
    hits: [
      {
        id: '1',
        created_at: new Date('2021-02-13T22:50:52.074Z'),
        title: '1',
        url: '1',
        author: '1',
        story_title: '1',
        story_url: '1',
        deleted: true,
      },
      {
        id: '2',
        created_at: new Date('2021-02-13T22:50:52.074Z'),
        title: '1',
        url: '1',
        author: '1',
        story_title: '1',
        story_url: '1',
        deleted: false,
      },
      {
        id: '3',
        created_at: new Date('2021-02-13T22:50:52.074Z'),
        title: '2',
        url: '2',
        author: '3',
        story_title: '5',
        story_url: '10',
        deleted: false,
      },
    ],
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(
          process.env.MONGO_HOST === 'atlas'
            ? MongoDBAtlas
            : MongoDBConnectionPath,
        ),
        MongooseModule.forFeature([{ name: 'Article', schema: ArticleSchema }]),
        HttpModule.register({
          timeout: 5000,
          maxRedirects: 5,
        }),
      ],
      providers: [ArticlesService],
    }).compile();
    articleService = moduleRef.get<ArticlesService>(ArticlesService);
  });

  describe('Defined modules', () => {
    it('ArticlesService should be defined', () => {
      expect(ArticlesService).toBeDefined();
    });
  });

  describe('articleService', () => {
    it('getArticles should return array', async () => {
      const spy = jest
        .spyOn(articleService, 'find')
        .mockResolvedValue(articlesArray as Article[]);
      const response = await articleService.getFormattedArticles();
      expect(spy).toBeCalledTimes(1);
      expect(response[0].id).toStrictEqual('2');
      jest.clearAllMocks();
    });
    it('deleteArticles should return an string', async () => {
      let result = 'Article deleted successfully';
      const spy = jest
        .spyOn(articleService, 'updateById')
        .mockImplementation(async (id, body) => ({ _id: '1' } as Article));
      const response = await articleService.deleteArticle('1');
      expect(spy).toBeCalledTimes(1);
      expect(response).toBe(result);
      jest.clearAllMocks();
    });
    it('insertArticles should return an string', async () => {
      const spy = jest
        .spyOn(articleService, 'insert')
        .mockImplementation(
          async (article: Article) => '1'
        );
      const response = await articleService.insertArticle({
        title: 'myTitle',
      } as Article);

      expect(spy).toBeCalledTimes(1);
      expect(response).toBe('1');
      jest.clearAllMocks();
    });
    it('populate should return text', async () => {
      const findSpy = jest
        .spyOn(articleService, 'find')
        .mockResolvedValue(articlesArray as Article[]);
      const fetchSpy = jest
        .spyOn(articleService, 'fetchArticlesFromHN')
        .mockResolvedValue(HNArticles as any);
      const insertSpy = jest
        .spyOn(articleService, 'insert')
        .mockResolvedValue('3');
      const response = await articleService.populateDB();
      expect(findSpy).toBeCalledTimes(1);
      expect(fetchSpy).toBeCalledTimes(1);
      expect(insertSpy).toBeCalledTimes(1);
      expect(response).toStrictEqual(1);
      jest.clearAllMocks();
    });
  });
});
