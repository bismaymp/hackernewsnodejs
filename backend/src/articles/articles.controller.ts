import { Controller, Get, Param, Put } from '@nestjs/common';

import { ArticlesService } from './articles.service';
import { FormattedArticle, Article } from './articles.model';

@Controller('api/articles')
export class ArticlesController {
  constructor(private readonly service: ArticlesService) {}

  @Get()
  async getFormattedArticles(): Promise<FormattedArticle[]>
  {
    const articles = await this.service.getFormattedArticles();
    return articles;
  }

  @Put(':id')
  async deleteArticle(@Param('id') id: string): Promise<String> {
    return await this.service.deleteArticle(id);
  }

  @Get('/populate')
  async populateDB(): Promise<number> {
      return await this.service.populateDB()
  }
}
