import { ArticlesModule } from './articles/articles.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import * as dotenv from 'dotenv';
import { ScheduleModule } from '@nestjs/schedule';
dotenv.config();

export const MongoDBConnectionPath = `mongodb://${process.env.MONGODB_URL}/${process.env.DB_NAME}`;
export const MongoDBAtlas =
  `mongodb+srv://bismay:SmcNn4ZLplgj3Kr5@cluster0.9af82.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`;

@Module({
  imports: [
    ArticlesModule,
    MongooseModule.forRoot(
      process.env.MONGO_HOST === 'atlas' ? MongoDBAtlas : MongoDBConnectionPath,
    ),
    ScheduleModule.forRoot()
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
