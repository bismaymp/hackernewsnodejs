# Backend

## Description

The project is set up to use Atlas(MongoDb), but you can use MongoDB renaming the file docker-compose-local.yml to docker-compose.yml

## Installation Manual(Development purpose)

```bash

cd backend
npm install


```

## Env variables

```

MONGODB_URL=localhost:27017/
DB_NAME=HackerNews
MONGO_HOST=atlas
PORT=8080

```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

# Frontend

## Installation Manual(Development purpose)

```bash

cd frontend
yarn


```

## Env variables

```

API=http://127.0.0.1:8080
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Docker Compose

```batch

docker-compose up --build

```
